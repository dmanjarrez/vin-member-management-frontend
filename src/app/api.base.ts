import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

export class ApiBase {
    protected transformOptions(options: any) {
        return Promise.resolve(options);
    }

    protected transformResult(url: string, response: HttpResponse<Blob>, processor: (response: HttpResponse<Blob>) => any): Observable<any> {
        return processor(response);
    }
}