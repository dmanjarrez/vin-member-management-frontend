import { Component, OnInit } from '@angular/core';
import { MembersClient, MemberSearchClient } from './services'
import { JsonResult, MemberModel, MemberSearchModel, FilterablePropertyModel, SortablePropertyModel } from './services';
import { Observable } from 'rxjs/Observable';
import { UUID } from 'angular2-uuid';
import * as moment from 'moment';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'members',
  templateUrl: './members.component.html',
  styleUrls: ['./members.component.scss'],
  providers: [ MembersClient, MemberSearchClient ]
})
export class MembersComponent implements OnInit {

  private propertyMapper = new MemberPropertyMapper();
  private emptyGuid: string = "";

  public filters = new Array<filter>();
  public filterComparerMapper = new FilterComparerMapper();

  public members = new Array<MemberModel>();
  public searchModel = new MemberSearchModel();
  public title = "Manage Members";

  public pageSizes = [2, 4, 10];
  public defaultPageSize = this.pageSizes[2];
  public selectedPageSize = this.defaultPageSize;
  public totalRecords = 0;

  public sortSettings = {
    "Last Name": "",
    "Date Of Birth": "",
    "Username": "",
    "Is Deleted": ""
  };
  
  constructor(
    private membersService: MembersClient,
    private memberSearchService: MemberSearchClient
  ) { }

  public ngOnInit() {
    this.initializeSearchModel();
    this.initializeFilters();
    this.changeSort(["Last Name", "First Name"]);
    this.doSearch();
  }

  public initializeFilters() {
    this.memberSearchService.getFilterableProperties().subscribe(r => { 
      let properties = r.data.FilterableProperties;
      let propertyNames = Object.keys(properties);
      propertyNames.forEach(n => {
        let propertyName = n;
        let allowedComparers: string[] = properties[n];
        let allowedFilterComparers = new Array<FilterComparer>();
        allowedComparers
          .forEach(c => { 
            let filterComparer = this.filterComparerMapper.getComparer(c);
            allowedFilterComparers.push(filterComparer);
        });
        allowedFilterComparers = allowedFilterComparers.sort((a, b) => FilterComparer.compare(a, b));
        this.filters.push(new filter(this.propertyMapper.getClientProperty(propertyName), allowedFilterComparers, null, false, null));
      });
    });
  }

  public doSearch() {
    this.memberSearchService.post(this.searchModel).subscribe(r => {
      this.members = r.data.Members;
      this.selectedPageSize = r.data.Paging.MaxPageSize;
      this.totalRecords = r.data.Paging.TotalRecords
    });
  }

  public changePage(newPage) {
    if(Number.isNaN(newPage))
      return;

    this.searchModel.pageNumber = newPage;
    this.doSearch();
  }

  public initializeSearchModel() {
    this.searchModel.filtering = new Array<FilterablePropertyModel>();
    this.searchModel.sorting = new Array<SortablePropertyModel>();
    this.searchModel.pageNumber = 1;
    this.searchModel.pageSize = this.defaultPageSize;
  }

  public changeSort(properties: string[]) {
    let property = properties[0];

    for(var key in this.sortSettings)
      if(key != property)
        this.sortSettings[key] = "";

    if(this.sortSettings[property] == "ascending")
      this.sortSettings[property] = "descending";
    else if(this.sortSettings[property] == "descending" || this.sortSettings[property] == "")
      this.sortSettings[property] = "ascending";

    this.searchModel.sorting = [];
    properties.forEach(property => {
      this.searchModel.sorting.push(
        new SortablePropertyModel({propertyName: this.propertyMapper.getApiProperty(property), direction: this.sortSettings[properties[0]] })
      );
    });

    this.doSearch();
  }

  public filtersOn() {
    return this.filters.filter(fs => fs.on).length > 0;
  }

  public turnOnFilters() {
    this.searchModel.filtering = new Array<FilterablePropertyModel>();
    this.filters.forEach(f => {
      if(!f.isValid())
        f.clear();
      else {
        this.searchModel.filtering.push(new FilterablePropertyModel({
          propertyName: this.propertyMapper.getApiProperty(f.propertyName), 
          comparer: f.selectedComparer.value,
          comparedValue: f.compareValue}));
        f.on = true;
      }
    });
      
    this.doSearch();
  }

  public turnOffFilters() {
    this.searchModel.filtering = new Array<FilterablePropertyModel>();
    this.filters.forEach(f => {
      f.clear();
    })

    this.doSearch();
  }

}

export class SortSetting {
  constructor(
    public property: string,
    public direction?: string
  )
  { }
}

export class PropertyMapping {
  constructor(
    public apiProperty: string,
    public displayProperty: string
  )
  { }
}

export class MemberPropertyMapper {
  constructor (
    private mappings = [
      new PropertyMapping("Lastname", "Last Name"),
      new PropertyMapping("Firstname", "First Name"),
      new PropertyMapping("DateOfBirth", "Date Of Birth"),
      new PropertyMapping("Username", "Username"),
      new PropertyMapping("EmailAddress", "Email"),
      new PropertyMapping("PhoneNumber", "Phone Number"),
      new PropertyMapping("IsDeleted", "Is Deleted"),
      new PropertyMapping("CreatedDate", "Created Date"),
      new PropertyMapping("LastUpdatedDate", "Last Updated Date")
    ]
  )
  { }

  public getApiProperty(clientProperty: string)
  {
    return this.mappings.filter(mapping => mapping.displayProperty == clientProperty)[0].apiProperty;
  }

  public getClientProperty(apiProperty: string)
  {
    return this.mappings.filter(mapping => mapping.apiProperty == apiProperty)[0].displayProperty;
  }
}

export class filter {
  constructor(
    public propertyName: string,
    public allowedComparers: FilterComparer[],
    public selectedComparer: FilterComparer,
    public on: boolean,
    public compareValue: string
  )
  { }

  public clear() {
    this.selectedComparer = null;
    this.on = false;
    this.compareValue = null;
  }

  public isValid() {
    return this.selectedComparer != null && !this.isEmptyOrWhitespace(this.compareValue);
  }

  public clearIfNotValid() {
    if(!this.isValid())
      this.clear();
  }

  private isEmptyOrWhitespace(str){
    return str == null || str.match(/^ *$/) !== null;
  }
}

export class FilterComparer {
  constructor(
    public value: string,
    public label: string,
    public order: number
  )
  { }

  public static compare = (a: FilterComparer, b: FilterComparer) => {
    return a.order < b.order 
      ? -1 
      : b.order < a.order 
        ? 1 
        : 0;
  }
}

export class FilterComparerMapper
{
  constructor(
    private valueToLabel = [
      new FilterComparer("", "", -1),
      new FilterComparer("lt", "Less Than", 0),
      new FilterComparer("lteq", "Less Than Or Equal To", 1 ),
      new FilterComparer("eq", "Equal To", 2),
      new FilterComparer("neq", "Not Equal To", 3),
      new FilterComparer("like", "Like", 4),
      new FilterComparer("gteq", "Greater Than Or Equal To", 5),
      new FilterComparer("gt", "Greater Than", 6)
    ]
  )
  {}

  public getComparer(value: string) {
    return this.valueToLabel.filter(l => l.value == value)[0];
  }
}